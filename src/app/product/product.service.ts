import { Injectable } from '@angular/core';

@Injectable()
export class ProductService {
	private productUrl: any;
	public productID: any;
	public priceID: any;
	public quantity: any;
	public priceValue: any;
	public productNameEn: any;
	public productNameVi: any;
	public brandName: any;
	public imgPath: any;
	constructor() { }

}
