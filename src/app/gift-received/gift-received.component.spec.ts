import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GiftReceivedComponent } from './gift-received.component';

describe('GiftReceivedComponent', () => {
  let component: GiftReceivedComponent;
  let fixture: ComponentFixture<GiftReceivedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GiftReceivedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GiftReceivedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
