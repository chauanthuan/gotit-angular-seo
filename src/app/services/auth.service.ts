import { Injectable } from '@angular/core';
import { Router } from "@angular/router";
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import { Observable } from 'rxjs/Observable';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import 'firebase/firestore';
@Injectable()
export class AuthService {

  private user: Observable<firebase.User>;
  	private userDetails: firebase.User = null;
  	private db:any;
	constructor(private _firebaseAuth: AngularFireAuth, private router: Router) { 
		this.db = firebase.firestore();
    	this.user = _firebaseAuth.authState;
    	this.user.subscribe(
	        (user) => {
	           	if (user) {
	            	this.userDetails = user;
	            }
	          	else {
	            	this.userDetails = null;
	          	}
	        }
	    );
  	}

  	signInWithGoogle() {
	    return this._firebaseAuth.auth.signInWithPopup(
	       	new firebase.auth.GoogleAuthProvider()
	    )
	}

	signInWithFacebook() {
	    return this._firebaseAuth.auth.signInWithPopup(
	       	new firebase.auth.FacebookAuthProvider()
	    )
	}

	loginRegular(email, password){
		return this._firebaseAuth.auth.signInWithEmailAndPassword(email, password);
	}

	signUpRegular(email, password) {
	    const credential = firebase.auth.EmailAuthProvider.credential( email, password );
	    return this._firebaseAuth.auth.createUserWithEmailAndPassword(email, password)
	}

	isLoggedIn():boolean {
		return this.userDetails !== null;
	}
	logout() {
	    this._firebaseAuth.auth.signOut()
	    .then((res) => {
	    	this.router.navigate(['/']);
	    	// localStorage.removeItem('currentUser');
	    });
	}

	getUserInfo(uid){
		return this.db.collection('user').doc(uid).collection("info").doc(uid);
	}

}
