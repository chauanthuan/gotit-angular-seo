import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { AngularFireAuthModule, AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';

@Component({
  selector: 'app-menu-left',
  templateUrl: './menu-left.component.html',
  styleUrls: ['./menu-left.component.scss']
})
export class MenuLeftComponent implements OnInit {
		userProviderLogin: any;
  constructor(private afAuth: AngularFireAuth) { }

  ngOnInit() {
  	 this.afAuth.authState.subscribe((user)=>{
		
		 	if(user){
		 		user.providerData.forEach((item)=>{
		 			if(item.providerId == 'password'){
		 				this.userProviderLogin = item.providerId;
		 				// console.log(this.userProviderLogin);
		 			}
		 		})
		 	}	 		
		})	
  }

}
