import { Component, OnInit,ViewChild, ElementRef, TemplateRef, Inject, PLATFORM_ID  } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import {MenuService} from '../menu/menu.service';
import { AngularFireAuthModule, AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import { Observable } from 'rxjs/Observable';
import {Router, ActivatedRoute} from '@angular/router';
import { Title }     from '@angular/platform-browser';
import {TranslateService,LangChangeEvent} from '@ngx-translate/core';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-email-verify',
  templateUrl: './email-verify.component.html',
  styleUrls: ['./email-verify.component.scss']
})
export class EmailVerifyComponent implements OnInit {

  isBtnActive:boolean = false;
  email:string;
  error:string;
  errorVerify: string;
  successVerify: string;
  messageSuccess:string;
  user: Observable<firebase.User>;
  confirmpassword:string;
  newpassword:string;
  confirmed:boolean = false;
  isLoading: boolean = false;
  oobCode: string;
  mode: string;

  private userDetails: firebase.User = null;
  constructor(private activatedRoute: ActivatedRoute, private router: Router,public _menuService: MenuService,private afAuth: AngularFireAuth,private titleService: Title,public translate: TranslateService,@Inject(PLATFORM_ID) private platformId: Object) { 
    this.activatedRoute.queryParams.subscribe(params => {
        this.oobCode = params['oobCode'];
        this.mode = params['mode'];
        if(this.mode == 'resetPassword'){
        	translate.get('resetpass_title').subscribe((res: string) => {
		        this.titleService.setTitle(res);
		    });
        }
        else if(this.mode == 'verifyEmail'){
        	translate.get('verifyEmail_title').subscribe((res: string) => {
		        this.titleService.setTitle(res);
		    });
        }
    });
  }

  ngOnInit() {
  	if (isPlatformBrowser(this.platformId)){
  		window.scrollTo(0, 0)
    	// console.log(firebase.auth());
	    this._menuService.show();
			let body = document.getElementsByTagName('body')[0];

	    if(!this.isBtnActive){
	    body.classList.remove('out');
	    }
	    else{
	    body.classList.add('out');
	    }
		let footer = document.getElementsByTagName('footer')[0];
		body.classList.add('email-verify');
		footer.classList.add('hide');
		body.click();
	    this.translate.onLangChange.subscribe((event: LangChangeEvent) => {
	      this.translate.get('resetpass_title').subscribe((res: string) => {
	        this.titleService.setTitle(res);
	      });
	    });
	}
    //check verify email 
    if(this.mode == 'verifyEmail'){
    	this.isLoading = true;
   		firebase.auth().applyActionCode(this.oobCode).then((resp) => {
	      	console.log(resp);
	      	this.isLoading = false;
		    if(this.translate.currentLang == 'vi'){
	  			this.successVerify = "Xin chúc mừng. Email của bạn đã được xác thực. Bây giờ bạn có thể đăng nhập với tài khoản của mình."
	  		}
	  		else{
	  			this.successVerify = "Congratulations. Your email has been verified. You can now log in with your account.";
	  		}
		  }).catch((errors) => {
		  	console.log(errors);
		  	this.isLoading = false;
		  	if(errors.code =='auth/invalid-action-code'){
		  		if(this.translate.currentLang == 'vi'){
		  			this.errorVerify = "Liên kết xác thực email của bạn đã hết hạn hoặc đã được xác thực."
		  		}
		  		else{
		  			this.errorVerify = "Your request to verify your email has expired or the link has already been used.";
		  		}
		  	}
		  });
    }
    
  }
  confirmPassType(event:any) { // without type info
  	  if (isPlatformBrowser(this.platformId)){
	    var target = event.currentTarget;
	    var idAttr = target.attributes.id.nodeValue;
	    if(idAttr == 'newpassword'){
	      this.newpassword = event.target.value;
	    }
	    else{
	      this.confirmpassword = event.target.value;
	    }
	    
	    if(this.confirmpassword == this.newpassword ){
	      this.confirmed = true;
	    }
	    else{
	      this.confirmed = false;
	    }
	    console.log(this.confirmed);
	  }
	}
  resetpass(){
  	this.isLoading = true;
  	firebase.auth().confirmPasswordReset(this.oobCode, this.newpassword).then(()=>{
  		this.isLoading = false;
  		console.log('tcong')
  		if(this.translate.currentLang == 'vi'){
			this.messageSuccess = 'Đặt lại mật khẩu thành công. Bây giờ bạn có thể đăng nhập với mật khẩu mới.';
		}
		else{
			this.messageSuccess = 'Reset password successfully. Now you can log in with new password.';
		}
  	}).catch((err)=>{
  		this.isLoading = false;
  		if(err.code == 'auth/invalid-action-code'){
  			if(this.translate.currentLang == 'vi'){
				this.error = 'Liên kết đặt lại mật khẩu đã hết hạn hoặc đã được thực hiện. Vui lòng kiểm tra lại.';
			}
			else{
				this.error = 'Your request to reset your password has expired or the link has already been used.';
			}
  		}
  		
  	})
  }

}
