export const listBrand = [ {
  "brandId" : 63,
  "brandImg" : "https://img.gotit.vn/compress/brand/1507286551_cmUef.png",
  "brandNm" : "Highlands Coffee",
  "categoryID" : [ 2 ],
  "slug" : "highlands-coffee",
  "sort" : 100
}, {
  "brandId" : 26,
  "brandImg" : "https://img.gotit.vn/compress/brand/1448707019_AZeJo.png",
  "brandNm" : "Phuc Long ",
  "categoryID" : [ 2 ],
  "slug" : "phuc-long",
  "sort" : 99
}, {
  "brandId" : 56,
  "brandImg" : "https://img.gotit.vn/compress/brand/1476931527_pEEGt.png",
  "brandNm" : "GoGi House",
  "categoryID" : [ 1 ],
  "slug" : "gogi-house",
  "sort" : 98
}, {
  "brandId" : 57,
  "brandImg" : "https://img.gotit.vn/compress/brand/1476931477_UzeYo.png",
  "brandNm" : "Kichi Kichi ",
  "categoryID" : [ 1 ],
  "slug" : "kichi-kichi",
  "sort" : 97
}, {
  "brandId" : 68,
  "brandImg" : "https://img.gotit.vn/compress/brand/1476349399_Hykct.png",
  "brandNm" : "SumoBBQ",
  "categoryID" : [ 1 ],
  "slug" : "sumobbq",
  "sort" : 96
}, {
  "brandId" : 50,
  "brandImg" : "https://img.gotit.vn/compress/brand/2016/06/1465199027_byUv9.png",
  "brandNm" : "Hoàng Yến Buffet",
  "categoryID" : [ 1 ],
  "slug" : "hoang-yen-buffet",
  "sort" : 95
}, {
  "brandId" : 101,
  "brandImg" : "https://img.gotit.vn/compress/brand/2017/06/1498656338_Xt8Y3.png",
  "brandNm" : "Lotte Mart",
  "categoryID" : [ 7 ],
  "slug" : "lotte-mart",
  "sort" : 94
}, {
  "brandId" : 6,
  "brandImg" : "https://img.gotit.vn/compress/brand/1519698965_soR4Y.png",
  "brandNm" : "TOUS les JOURS",
  "categoryID" : [ 2 ],
  "slug" : "tous-les-jours",
  "sort" : 93
}, {
  "brandId" : 35,
  "brandImg" : "https://img.gotit.vn/brand/1453994997_xnjWC.png",
  "brandNm" : "BreadTalk",
  "categoryID" : [ 2 ],
  "slug" : "breadtalk",
  "sort" : 92
}, {
  "brandId" : 106,
  "brandImg" : "https://img.gotit.vn/compress/brand/2017/10/1507190231_lWGfl.png",
  "brandNm" : "John Henry",
  "categoryID" : [ 3 ],
  "slug" : "john-henry",
  "sort" : 91
}, {
  "brandId" : 13,
  "brandImg" : "https://img.gotit.vn/compress/brand/2015/10/1446109576_QHSHR.png",
  "brandNm" : "JOCKEY",
  "categoryID" : [ 3 ],
  "slug" : "jockey",
  "sort" : 90
}, {
  "brandId" : 91,
  "brandImg" : "https://img.gotit.vn/compress/brand/2017/02/1487923827_yNVCf.png",
  "brandNm" : "Ministop",
  "categoryID" : [ 7 ],
  "slug" : "ministop",
  "sort" : 89
}, {
  "brandId" : 41,
  "brandImg" : "https://img.gotit.vn/compress/brand/1470128103_0bgUA.png",
  "brandNm" : "LOCK&LOCK ",
  "categoryID" : [ 7 ],
  "slug" : "locklock",
  "sort" : 88
}, {
  "brandId" : 19,
  "brandImg" : "https://img.gotit.vn/compress/brand/2015/11/1446633339_f85i3.png",
  "brandNm" : "Baskin Robbins",
  "categoryID" : [ 2 ],
  "slug" : "baskin-robbins",
  "sort" : 87
}, {
  "brandId" : 110,
  "brandImg" : "https://img.gotit.vn/compress/brand/2018/01/1515644472_tlqht.png",
  "brandNm" : "BoBaPoP",
  "categoryID" : [ 2 ],
  "slug" : "bobapop",
  "sort" : 86
}, {
  "brandId" : 72,
  "brandImg" : "https://img.gotit.vn/compress/brand/1479370699_B6YWt.png",
  "brandNm" : "iSushi - Buffet Nhật Bản",
  "categoryID" : [ 1 ],
  "slug" : "isushi-buffet-japanese",
  "sort" : 85
}, {
  "brandId" : 70,
  "brandImg" : "https://img.gotit.vn/compress/brand/1476931339_X80IM.png",
  "brandNm" : "Cowboy Jack's",
  "categoryID" : [ 1 ],
  "slug" : "cowboy-jacks",
  "sort" : 84
}, {
  "brandId" : 43,
  "brandImg" : "https://img.gotit.vn/compress/brand/1468227543_adilc.png",
  "brandNm" : "Yves Rocher France",
  "categoryID" : [ 3, 8 ],
  "slug" : "yves-rocher-france",
  "sort" : 83
}, {
  "brandId" : 14,
  "brandImg" : "https://img.gotit.vn/compress/brand/2015/10/1446109589_db9GA.png",
  "brandNm" : "VERA",
  "categoryID" : [ 3 ],
  "slug" : "vera",
  "sort" : 82
}, {
  "brandId" : 7,
  "brandImg" : "https://img.gotit.vn/compress/brand/2015/10/1445251857_UZ373.png",
  "brandNm" : "SHOP & GO",
  "categoryID" : [ 7 ],
  "slug" : "shop-go",
  "sort" : 81
}, {
  "brandId" : 20,
  "brandImg" : "https://img.gotit.vn/compress/brand/1492658268_29LMD.png",
  "brandNm" : "ThaiExpress ",
  "categoryID" : [ 1 ],
  "slug" : "thaiexpress",
  "sort" : 80
}, {
  "brandId" : 4,
  "brandImg" : "https://img.gotit.vn/compress/brand/2015/10/1444999138_1FwXS.png",
  "brandNm" : "THE SUSHI BAR",
  "categoryID" : [ 1 ],
  "slug" : "the-sushi-bar",
  "sort" : 79
}, {
  "brandId" : 81,
  "brandImg" : "https://img.gotit.vn/compress/brand/1479369994_WdlOZ.png",
  "brandNm" : "Hutong",
  "categoryID" : [ 1 ],
  "slug" : "hutong",
  "sort" : 78
}, {
  "brandId" : 67,
  "brandImg" : "https://img.gotit.vn/compress/brand/1476931239_cwmPO.png",
  "brandNm" : "Vuvuzela Beer Club",
  "categoryID" : [ 1 ],
  "slug" : "vuvuzela-beer-club",
  "sort" : 77
}, {
  "brandId" : 49,
  "brandImg" : "https://img.gotit.vn/compress/brand/2016/06/1465198975_0Yshs.png",
  "brandNm" : "Hoàng Yến Vietnamese Cuisine",
  "categoryID" : [ 1 ],
  "slug" : "hoang-yen-vietnamese-cuisine",
  "sort" : 76
}, {
  "brandId" : 15,
  "brandImg" : "https://img.gotit.vn/compress/brand/2015/10/1446109611_jGqEy.png",
  "brandNm" : "WOW",
  "categoryID" : [ 3 ],
  "slug" : "wow",
  "sort" : 75
}, {
  "brandId" : 94,
  "brandImg" : "https://img.gotit.vn/compress/brand/2017/03/1489379200_pnzkb.png",
  "brandNm" : "Taka",
  "categoryID" : [ 7 ],
  "slug" : "taka",
  "sort" : 74
}, {
  "brandId" : 51,
  "brandImg" : "https://img.gotit.vn/compress/brand/2016/06/1465199085_P0gZ1.png",
  "brandNm" : "Hoàng Yến Buffet Premier",
  "categoryID" : [ 1 ],
  "slug" : "hoang-yen-buffet-premier",
  "sort" : 73
}, {
  "brandId" : 52,
  "brandImg" : "https://img.gotit.vn/compress/brand/2016/06/1465199213_akgVB.png",
  "brandNm" : "Hoàng Yến Hot Pot",
  "categoryID" : [ 1 ],
  "slug" : "hoang-yen-hot-pot",
  "sort" : 72
}, {
  "brandId" : 108,
  "brandImg" : "https://img.gotit.vn/compress/brand/2017/10/1507190609_xBD1F.png",
  "brandNm" : "Aino Sofia",
  "categoryID" : [ 3 ],
  "slug" : "aino-sofia",
  "sort" : 71
}, {
  "brandId" : 107,
  "brandImg" : "https://img.gotit.vn/compress/brand/2017/10/1507190536_qz9nY.png",
  "brandNm" : "Freelancer",
  "categoryID" : [ 3 ],
  "slug" : "freelancer",
  "sort" : 70
}, {
  "brandId" : 54,
  "brandImg" : "https://img.gotit.vn/compress/brand/2016/06/1465392465_7mdJ9.png",
  "brandNm" : "ShooZ",
  "categoryID" : [ 3 ],
  "slug" : "shooz",
  "sort" : 69
}, {
  "brandId" : 44,
  "brandImg" : "https://img.gotit.vn/compress/brand/1465469787_nTG9S.png",
  "brandNm" : "Geox ",
  "categoryID" : [ 3 ],
  "slug" : "geox",
  "sort" : 68
}, {
  "brandId" : 104,
  "brandImg" : "https://img.gotit.vn/compress/brand/2017/09/1505458817_qTTXP.png",
  "brandNm" : "Flormar",
  "categoryID" : [ 3 ],
  "slug" : "flormar",
  "sort" : 67
}, {
  "brandId" : 65,
  "brandImg" : "https://img.gotit.vn/compress/brand/1479371366_rYT0T.png",
  "brandNm" : "Daruma",
  "categoryID" : [ 1 ],
  "slug" : "daruma",
  "sort" : 66
}, {
  "brandId" : 64,
  "brandImg" : "https://img.gotit.vn/compress/brand/1479371417_KI3PQ.png",
  "brandNm" : "Ashima",
  "categoryID" : [ 1 ],
  "slug" : "ashima",
  "sort" : 65
}, {
  "brandId" : 58,
  "brandImg" : "https://img.gotit.vn/compress/brand/2016/08/1470871968_qzmnG.png",
  "brandNm" : "Ding Tea",
  "categoryID" : [ 2 ],
  "slug" : "ding-tea",
  "sort" : 64
}, {
  "brandId" : 75,
  "brandImg" : "https://img.gotit.vn/compress/brand/1479370553_vJzKH.png",
  "brandNm" : "Osaka Ohsho",
  "categoryID" : [ 1 ],
  "slug" : "osaka-ohsho",
  "sort" : 63
}, {
  "brandId" : 71,
  "brandImg" : "https://img.gotit.vn/compress/brand/1479370747_c3h66.png",
  "brandNm" : "K-Pub - Korean BBQ Garden",
  "categoryID" : [ 1 ],
  "slug" : "k-pub-korean-bbq-garden",
  "sort" : 62
}, {
  "brandId" : 88,
  "brandImg" : "https://img.gotit.vn/compress/brand/2017/01/1483427701_gdVgf.png",
  "brandNm" : "Shogun – Japanese Street BBQ",
  "categoryID" : [ 1 ],
  "slug" : "shogun-japanese-street-bbq",
  "sort" : 61
}, {
  "brandId" : 79,
  "brandImg" : "https://img.gotit.vn/compress/brand/1479370262_Ybk3w.png",
  "brandNm" : "Phố Ngon 37",
  "categoryID" : [ 1 ],
  "slug" : "37street",
  "sort" : 60
}, {
  "brandId" : 66,
  "brandImg" : "https://img.gotit.vn/compress/brand/1476930969_ZBKUI.png",
  "brandNm" : "Ba Con Cừu - Lẩu Cừu Mông Cổ",
  "categoryID" : [ 1 ],
  "slug" : "ba-con-cuu-mongolian-hot-pot",
  "sort" : 59
}, {
  "brandId" : 113,
  "brandImg" : "https://img.gotit.vn/compress/brand/1522211755_XKB5o.png",
  "brandNm" : "Hokkaido Baked Cheese Tart",
  "categoryID" : [ 2 ],
  "slug" : "hokkaido-baked-cheese-tart",
  "sort" : 58
}, {
  "brandId" : 92,
  "brandImg" : "https://img.gotit.vn/compress/brand/2017/02/1488275232_RwCtN.png",
  "brandNm" : "Smoothie Factory",
  "categoryID" : [ 2 ],
  "slug" : "smoothie-factory",
  "sort" : 57
}, {
  "brandId" : 39,
  "brandImg" : "https://img.gotit.vn/brand/2016/02/1454347302_7opvi.png",
  "brandNm" : "Beard Papa's",
  "categoryID" : [ 2 ],
  "slug" : "beard-papas",
  "sort" : 56
}, {
  "brandId" : 78,
  "brandImg" : "https://img.gotit.vn/compress/brand/1479370317_UXCSb.png",
  "brandNm" : "City Beer Station",
  "categoryID" : [ 1 ],
  "slug" : "city-beer-station",
  "sort" : 55
}, {
  "brandId" : 73,
  "brandImg" : "https://img.gotit.vn/compress/brand/1479370681_AKGK4.png",
  "brandNm" : "Crystal Jade Kitchen",
  "categoryID" : [ 1 ],
  "slug" : "crystal-jade-kitchen",
  "sort" : 54
}, {
  "brandId" : 21,
  "brandImg" : "https://img.gotit.vn/compress/brand/2015/11/1447041627_H2Jgo.png",
  "brandNm" : "Ngõ – Saigon Street Cafe",
  "categoryID" : [ 1 ],
  "slug" : "ngo-saigon-street-cafe",
  "sort" : 53
}, {
  "brandId" : 2,
  "brandImg" : "https://img.gotit.vn/compress/brand/1444997784_5AoBb.png",
  "brandNm" : "GLOW SKYBAR",
  "categoryID" : [ 1 ],
  "slug" : "glow-skybar",
  "sort" : 52
}, {
  "brandId" : 3,
  "brandImg" : "https://img.gotit.vn/compress/brand/1444998114_FMp4P.png",
  "brandNm" : "URBAN KITCHEN + BAR",
  "categoryID" : [ 1 ],
  "slug" : "urban-kitchen-bar",
  "sort" : 51
}, {
  "brandId" : 22,
  "brandImg" : "https://img.gotit.vn/compress/brand/2015/11/1447041698_Uw5p5.png",
  "brandNm" : "Shabu Ya",
  "categoryID" : [ 1 ],
  "slug" : "shabu-ya",
  "sort" : 50
}, {
  "brandId" : 86,
  "brandImg" : "https://img.gotit.vn/compress/brand/1481766127_VeaUY.png",
  "brandNm" : "Spice Temple",
  "categoryID" : [ 1 ],
  "slug" : "spice-temple",
  "sort" : 49
}, {
  "brandId" : 109,
  "brandImg" : "https://img.gotit.vn/compress/brand/1516349430_DZxb2.png",
  "brandNm" : "3Sạch Food",
  "categoryID" : [ 7 ],
  "slug" : "3sach-food",
  "sort" : 48
}, {
  "brandId" : 53,
  "brandImg" : "https://img.gotit.vn/compress/brand/2016/06/1465199401_Nw0qx.png",
  "brandNm" : "Stix - Dine Wine Cafe",
  "categoryID" : [ 1 ],
  "slug" : "stix-dine-wine-cafe",
  "sort" : 47
}, {
  "brandId" : 112,
  "brandImg" : "https://img.gotit.vn/compress/brand/1520392094_INDXc.png",
  "brandNm" : "Manwah Taiwanese Hotpot",
  "categoryID" : [ 1 ],
  "slug" : "manwah-taiwanese-hotpot",
  "sort" : 46
}, {
  "brandId" : 82,
  "brandImg" : "https://img.gotit.vn/compress/brand/1479369764_rMSQG.png",
  "brandNm" : "Sừng Quăn",
  "categoryID" : [ 1 ],
  "slug" : "sung-quan",
  "sort" : 45
}, {
  "brandId" : 74,
  "brandImg" : "https://img.gotit.vn/compress/brand/1479370594_QsAe6.png",
  "brandNm" : "iCook - Restaurant at Home",
  "categoryID" : [ 1 ],
  "slug" : "icook-restaurant-at-home",
  "sort" : 44
}, {
  "brandId" : 80,
  "brandImg" : "https://img.gotit.vn/compress/brand/1479370039_YPVDG.png",
  "brandNm" : "Itacho Ramen",
  "categoryID" : [ 1 ],
  "slug" : "itacho-ramen",
  "sort" : 43
}, {
  "brandId" : 77,
  "brandImg" : "https://img.gotit.vn/compress/brand/1479370431_ammal.png",
  "brandNm" : "Kintaro Udon",
  "categoryID" : [ 1 ],
  "slug" : "kintaro-udon",
  "sort" : 42
}, {
  "brandId" : 76,
  "brandImg" : "https://img.gotit.vn/compress/brand/1479370534_Amdo6.png",
  "brandNm" : "Magic Pan",
  "categoryID" : [ 1 ],
  "slug" : "magic-pan",
  "sort" : 41
}, {
  "brandId" : 37,
  "brandImg" : "https://img.gotit.vn/brand/1454397416_tZ9Kz.png",
  "brandNm" : "Plus One Healthy Supplements",
  "categoryID" : [ 7 ],
  "slug" : "plus-one-healthy-supplements",
  "sort" : 40
}, {
  "brandId" : 27,
  "brandImg" : "https://img.gotit.vn/compress/brand/1493017918_zLkdv.png",
  "brandNm" : "Catherine Denoual Maison",
  "categoryID" : [ 8 ],
  "slug" : "catherine-denoual-maison",
  "sort" : 39
}, {
  "brandId" : 93,
  "brandImg" : "https://img.gotit.vn/compress/brand/1492509366_xiMR6.png",
  "brandNm" : "Hoa Yêu Thương",
  "categoryID" : [ 8 ],
  "slug" : "hoa-yeu-thuong",
  "sort" : 38
}, {
  "brandId" : 95,
  "brandImg" : "https://img.gotit.vn/compress/brand/2017/04/1492500172_sNWDr.png",
  "brandNm" : "KARUIZAWA - SHIRT",
  "categoryID" : [ 3 ],
  "slug" : "karuizawa-shirt",
  "sort" : 36
}, {
  "brandId" : 98,
  "brandImg" : "https://img.gotit.vn/compress/brand/2017/05/1495428029_69HLa.png",
  "brandNm" : "REGAL TOKYO ",
  "categoryID" : [ 3 ],
  "slug" : "regal-tokyo",
  "sort" : 35
}, {
  "brandId" : 84,
  "brandImg" : "https://img.gotit.vn/compress/brand/2016/11/1478257486_kFinf.png",
  "brandNm" : "Tập Đoàn Y Khoa Hoàn Mỹ",
  "categoryID" : [ 8 ],
  "slug" : "hoan-my-medical-coporation",
  "sort" : 34
}, {
  "brandId" : 69,
  "brandImg" : "https://img.gotit.vn/compress/brand/2016/10/1476345667_A3ssm.png",
  "brandNm" : "Golden Gate Group",
  "categoryID" : [ 1 ],
  "slug" : "golden-gate-group",
  "sort" : 0
}, {
  "brandId" : 46,
  "brandImg" : "https://img.gotit.vn/compress/brand/1521448379_G4uI2.png",
  "brandNm" : "Got It",
  "categoryID" : [ 2, 8 ],
  "slug" : "got-it",
  "sort" : 0
}, {
  "brandId" : 55,
  "brandImg" : "https://img.gotit.vn/compress/brand/1470302517_7RqdO.png",
  "brandNm" : "Hoàng Yến Group",
  "categoryID" : [ 1 ],
  "slug" : "hoang-yen-group",
  "sort" : 0
}];
