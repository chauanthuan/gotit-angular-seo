import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GiftSentComponent } from './gift-sent.component';

describe('GiftSentComponent', () => {
  let component: GiftSentComponent;
  let fixture: ComponentFixture<GiftSentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GiftSentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GiftSentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
