import { Component, OnInit,ViewChild, ElementRef, Inject, PLATFORM_ID  } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { TranslateService } from '@ngx-translate/core';
import {ModalDirective} from 'ngx-bootstrap/modal';
declare var $: any;
// import { AngularFireAuth } from 'angularfire2/auth';
// import { AngularFireDatabase} from 'angularfire2/database';

// import { AngularFireObject } from 'angularfire2/database';

// import * as firebase from "firebase";
import {MenuService} from './menu.service';
import { Observable } from 'rxjs/Observable';
import { AngularFireAuthModule, AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import {Router, ActivatedRoute} from '@angular/router';
import { AuthService } from '../services/auth.service';
declare var bootbox:any;

@Component({
  selector: 'menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
  host: {
    '(window:resize)': 'onResize($event)'
  }
})
export class MenuComponent implements OnInit {
  isBtnActive:boolean = false;
  user: Observable<firebase.User>;
  // test:AngularFireObject<any>;
  isMobile:boolean = false;
  fullname:string;
  errorLogin:string;
  isLoading:boolean = false;
  returnUrl: string;
  passwordRequired: string;
  email: string;
  password:string;
  errRequireVerify: boolean = false;
  userData: any;
  messageSuccess: string;
  db: any;
  constructor(
    // public db: AngularFireDatabase, 
    public afAuth: AngularFireAuth,
    public _menuService: MenuService,
    private router: Router,
    public translate: TranslateService,
    public authService: AuthService,
    private route: ActivatedRoute,
    @Inject(PLATFORM_ID) private platformId: Object
    ) {
    this.user = this.afAuth.authState;
  }

  ngOnInit() {
      if (isPlatformBrowser(this.platformId)){
        var w_width = $(window).width();
        if(w_width <= 480){
          this.isMobile = true;
        }
      }

      this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';

      this.afAuth.authState.subscribe((user)=>{
        if(user){
          firebase.firestore().collection('user').doc(user.uid).onSnapshot((snapshot)=>{
            if(snapshot.exists){
              let user_firebase = snapshot.data();
              let firstname = user_firebase.fname;
              let lastname = user_firebase.lname;
              this.fullname = firstname + ' ' + lastname;
            }
          });
        }
      })
  }
  setLanguage(val){
    this.translate.use(val);
    localStorage.setItem("CurrentLanguage",val);
    this.isBtnActive = !this.isBtnActive;
    let body = document.getElementsByTagName('body')[0];
    if(!this.isBtnActive){
      body.classList.remove('out');
    }
    else{
      body.classList.add('out');
    }
  }
  onResize(event){
    if(event.target.innerWidth <= 480){
      this.isBtnActive = false;
      this.isMobile = true;
      let body = document.getElementsByTagName('body')[0];
      body.classList.remove('out');
    }
    else{
      this.isMobile = false;
    }
  }

  // logout() {
  //   this.afAuth.auth.signOut();
  //   localStorage.removeItem('currentUser');
  //   this.router.navigate(['/login']);
  // }

  toggleMenu(event):void{
    this.isBtnActive = !this.isBtnActive;
    let body = document.getElementsByTagName('body')[0];
    if(!this.isBtnActive){
      body.classList.remove('out');
    }
    else{
      body.classList.add('out');
    }
  }

  update(value) {
    // this.test.set(value);
  }

  
  logout(){  
    this.staticModal.hide();
    this.authService.logout();
    this.isMobile = true;
    this.isBtnActive = false;
    let body = document.getElementsByTagName('body')[0];
    body.classList.remove('out');

  }
 
  showLoginModal(){

    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    this.staticLoginModal.show();
    let body = document.getElementsByTagName('body')[0];
    body.click();
  }
  login(){
    this.staticLoginModal.show();
  }

  showlogoutmodal(){
     this.staticModal.show();
  }
  hidelogoutmodal(){
    this.staticModal.hide();
  }

   @ViewChild('staticModal') public staticModal: ModalDirective;
   @ViewChild('staticLoginModal') public staticLoginModal: ModalDirective;
   @ViewChild('staticEnterPassModal') public staticEnterPassModal: ModalDirective;
   @ViewChild('passwordRequiredInput') focusInput:ElementRef;

   signInWithFacebook() {
      this.isLoading = true;
    this.authService.signInWithFacebook()
    .then((authData) => { 
      var userProfile = authData.additionalUserInfo.profile;
      var db = firebase.firestore();    
      let userData = authData.user; 

      let birthday = '';
      console.log(userProfile);
      if(userProfile.birthday){
        var splitted = userProfile.birthday.split("/");//mm/dd/yyyy
        birthday = splitted[2]  + '-' + splitted[0]+ '-' + splitted[1];//to yyyy-mm-dd
      }
      db.collection("user").doc(userData.uid).get().then((response)=>{
        if(!response || !response.exists){
          db.collection("user").doc(userData.uid).set({
            phone: userData.phoneNumber,
            fname: userProfile.first_name,
            lname:userProfile.last_name,
            gender: (userProfile.gender == 'male') ? 'M':'F',
            birthday:birthday
          }).then(() => {
              console.log("Document successfully written!");
          })
          .catch((error) => {
              console.error("Error writing document: ", error);
          });
        }
        else{
          db.collection("user").doc(userData.uid).update({
            phone: response.data().phone ? response.data().phone : userData.phoneNumber,
            fname: response.data().fname ? response.data().fname :  userProfile.first_name,
            lname: response.data().lname ? response.data().lname : userProfile.last_name,
            gender: response.data().gender ? response.data().gender : (userProfile.gender == 'male') ? 'M':'F',
            birthday:response.data().birthday ? response.data().birthday :  birthday
          }).then(() => {
              console.log("Document successfully update!");
          })
          .catch((error) => {
              console.error("Error update document: ", error);
          });
        }
      }).catch((error)=>{
      console.log(error);
      })
      this.isLoading = false;
      this.staticLoginModal.hide();
      this.router.navigateByUrl(this.returnUrl);
    })
    .catch((error) => {
      console.log(error);
      if(error.code === 'auth/account-exists-with-different-credential' )
      {
        var pendingCred = error.credential;
        // The provider account's email address.
        var email = error.email;
        firebase.auth().fetchProvidersForEmail(email).then((providers)=>{
          if (providers[0] === 'password') {
            this.passwordRequired = '';
            this.staticLoginModal.hide();
            //this.staticEnterPassModal.show();
            let title_box = '';
            let message_box = '';
            if(this.translate.currentLang == 'vi'){
              title_box = "Nhập mật khẩu";
              message_box = "Email trong FB này đã được đăng kí với hệ thống bằng phương thức truyền thống (email và mật khẩu). Vui lòng nhập mật khẩu của tài khoản này.";
            }
            else{
              title_box = "Enter your password";
              message_box = "An account already exists with the same email address but different sign-in credentials. Please enter your password with this email address.";
            }


            bootbox.prompt({
              title: title_box,
              inputType: 'password',
              buttons: {
                cancel: {
                  label: this.translate.currentLang == 'vi' ? 'Đóng' : 'Cancel',
                  className: 'btn-default',
                },
                confirm: {
                  label: this.translate.currentLang == 'vi' ? 'Xác nhận' : 'Confirm',
                  className: 'btn-success',
                }
              },
              callback:  (password) => {
                  if(password === null){
                    console.log('click Close btm')
                  }
                  else{
                    if(password != ''){
                     this.authService.loginRegular(email, password )
                      .then((authData) => {
                        console.log(authData);
                        return authData.linkWithCredential(pendingCred).then(()=>{
                          this.isLoading = false
                          this.router.navigateByUrl(this.returnUrl);
                          $('.bootbox').modal('hide');
                        })
                        
                      })
                      .catch((e) => {
                        console.log(e);
                        if(e.code == 'auth/wrong-password'){
                          if(this.translate.currentLang == 'vi'){
                            this.errorLogin = 'Mật khẩu của bạn không chính xác';
                          }
                          else{
                            this.errorLogin = 'Your password is incorrect.';
                          }
                        }
                        else{
                          this.errorLogin = e.message;
                        }
                        
                        this.isLoading = false;
                        $('.bootbox-input').val('');
                        $('.bootbox-body').find('.err').html('').prepend('<p class="err" style="color:#ff5f5f;">'+this.errorLogin+'</p>');

                        return false;
                      });
                    return false;
                   }
                  else{
                   if(this.translate.currentLang == 'vi'){
                      this.errorLogin = 'Vui lòng nhập mật khẩu.';
                    }
                    else{
                      this.errorLogin = 'Please enter your password.';
                    }
                    $('.bootbox-body').find('.err').html('').prepend('<p class="err" style="color:#ff5f5f;">'+this.errorLogin+'</p>');

                    return false;
                 }
               }
              }
          }).find('.bootbox-body').prepend('<p>'+message_box+'</p><div class="err"></div>');

          }
          else if(providers[0] == 'google.com'){
            firebase.auth().signInWithPopup(new firebase.auth.GoogleAuthProvider())
            .then((result) => {
                result.user.linkWithCredential(error.credential)
                .catch((error)=>{
                    this.errorLogin = error.message;
                });
                // localStorage.setItem('currentUser', JSON.stringify(result.user.uid));
                this.staticLoginModal.hide();
                this.router.navigateByUrl(this.returnUrl);
            });
          }
        });
      }
      else {
        this.isLoading = false;
        this.errorLogin = error.message;
        if(error.code == 'auth/cancelled-popup-request' || error.code == 'auth/popup-closed-by-user'){
          this.errorLogin = '';
        }
        if(error.code == 'auth/network-request-failed'){
          if(this.translate.currentLang == 'vi'){
            this.errorLogin = 'Lỗi mạng. Vui lòng thử lại sau.';
          }
          else{
            this.errorLogin = 'A network error. Please try again later.';
          }
        }
      }
    });
    }

    signInWithGoogle(){
      this.isLoading = true;
      this.authService.signInWithGoogle()
    .then((authData) => { 
      console.log(authData);
      var userProfile = authData.additionalUserInfo.profile;
      var db = firebase.firestore();    
      let userData = authData.user; 
      let birthday = '';
      if(userProfile.birthday){
        var splitted = userProfile.birthday.split("/");
        birthday = splitted[2] + '-' + splitted[1] + '-' + splitted[0];
      }
      db.collection("user").doc(userData.uid).get().then((response)=>{
      if(!response || !response.exists){
        db.collection("user").doc(userData.uid).set({
          phone: userData.phoneNumber,
          fname: userProfile.given_name,
          lname:userProfile.family_name,
          gender: (userProfile.gender == 'male') ? 'M':'F',
          birthday:birthday
        }).then(() => {
            console.log("Document successfully written!");
        })
        .catch((error) => {
            console.error("Error writing document: ", error);
        });
      }
      else{
        db.collection("user").doc(userData.uid).update({
          phone: response.data().phone ? response.data().phone : userData.phoneNumber,
          fname: response.data().fname ? response.data().fname :  userProfile.given_name,
          lname: response.data().lname ? response.data().lname : userProfile.family_name,
          gender: response.data().gender ? response.data().gender : (userProfile.gender == 'male') ? 'M':'F',
          birthday:response.data().birthday ? response.data().birthday :  birthday
        }).then(() => {
            console.log("Document successfully update!");
        })
        .catch((error) => {
            console.error("Error update document: ", error);
        });
      }
      }).catch((error)=>{
      console.log(error);
      })
        this.isLoading = false;
        this.staticLoginModal.hide();
        this.router.navigateByUrl(this.returnUrl);
    })
    .catch((error) => {
        console.log(error);
        this.isLoading = false;
        
        if(error.code == 'auth/cancelled-popup-request' || error.code == 'auth/popup-closed-by-user'){
          this.errorLogin = '';
        }
        else if(error.code == 'auth/network-request-failed'){
          if(this.translate.currentLang == 'vi'){
            this.errorLogin = 'Lỗi mạng. Vui lòng thử lại sau.';
          }
          else{
            this.errorLogin = 'A network error. Please try again later.';
          }
        }
        else{
          this.errorLogin = error.message;
        }
    });
  }

  loginRegular() {
      this.isLoading = true;
        this.authService.loginRegular(this.email, this.password)
        .then((authData) => {
          console.log(authData);
          this.userData = authData;
          this.isLoading = false;
          if(authData.emailVerified){
            this.staticLoginModal.hide();
            this.router.navigateByUrl(this.returnUrl);
          }
          else{
            this.errRequireVerify = true;
            if(this.translate.currentLang == 'vi'){
              this.errorLogin = 'Vui lòng xác thực địa chỉ email của bạn trước khi đăng nhập.';
            }
            else{
              this.errorLogin = 'Please verify your email address before login and try again.';
            }
            firebase.auth().signOut();
            return;   
          }    
         //this.router.navigate(['/']);
        })
        .catch((error) => {
          console.log(error);
          this.isLoading = false;
          if( error.code == 'auth/wrong-password' || error.code == 'auth/user-not-found'){
            if(this.translate.currentLang == 'vi'){
              this.errorLogin = 'Tên đăng nhập hoặc mật khẩu của bạn không chính xác';
            }
            else{
              this.errorLogin = 'Your username or password is incorrect.';
            }
          }         
          else if(error.code == 'auth/cancelled-popup-request' || error.code == 'auth/popup-closed-by-user'){
            this.errorLogin = '';
          } 
          else if(error.code == 'auth/network-request-failed'){
            if(this.translate.currentLang == 'vi'){
              this.errorLogin = 'Lỗi mạng. Vui lòng thử lại sau.';
            }
            else{
              this.errorLogin = 'A network error. Please try again later.';
            }
          }
          else{
            this.errorLogin = error.message;
          } 
        });
    }

    sendVerifyEmailAgain(){

      this.userData.sendEmailVerification().then(() => {
        this.errorLogin = '';
        this.errRequireVerify = false;
        if(this.translate.currentLang == 'vi'){
          this.messageSuccess = 'Vui lòng kiểm tra email của bạn.';
        }
        else{
          this.messageSuccess = 'Please check your email.';
        }
      })
    }
}
